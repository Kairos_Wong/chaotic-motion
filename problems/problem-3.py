import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


# Constants
g = 9.81

# The parametrized function to be plotted
def theta_t(t, Theta_0, L_0, Q_0):
    beta=Q_0/2
    omega=np.sqrt(g/L_0)

    if beta >= omega:
        return Theta_0 * np.exp(-beta * t) * np.cosh(np.sqrt(np.absolute(pow(beta,2) - pow(omega,2)))*t)
    else:
        return Theta_0 * np.exp(-beta * t) * np.cos(np.sqrt(np.absolute(pow(beta,2) - pow(omega,2)))*t)
        

# Define initial parameters
t = np.linspace(0, 100, 10000)
Q_0 = 1
L_0 = .5
Theta_0 = 5

# Create the figure and the line that we will manipulate
fig, ax = plt.subplots()
plt.ylim([-5,5])
plt.xlim([0,5])

line, = ax.plot(t, theta_t(t, Theta_0, L_0, Q_0), lw=1)
ax.set_xlabel('Time (s)')

# adjust the main plot to make room for the sliders
fig.subplots_adjust(left=0.25, bottom=0.25)

# Make a vertically oriented slider to control the amplitude
axTheta = fig.add_axes([0.1, 0.25, 0.0225, 0.63])
Theta_slider = Slider(
    ax=axTheta,
    label="Initial Angle",
    valmin=0,
    valmax=10,
    valinit=Theta_0,
    orientation="vertical"
)

# Make a horizontal sliders to control pendulum length and q
axL = fig.add_axes([0.25, 0.05, 0.65, 0.03])
L_slider = Slider(
    ax=axL,
    label='Length (m)',
    valmin=.01,
    valmax=.5,
    valinit=L_0,
)

axQ = fig.add_axes([0.25, 0.10, 0.65, 0.03])
Q_slider = Slider(
    ax=axQ,
    label='Dampening Coefficient',
    valmin=1,
    valmax=15,
    valinit=Q_0*1.75,
)

# The function to be called anytime a slider's value changes
def update(val):
    line.set_ydata(theta_t(t, Theta_slider.val, L_slider.val, Q_slider.val))
    line.set_ydata(exp_t(t, Theta_slider.val, L_slider.val, Q_slider.val))
    fig.canvas.draw_idle()


# register the update function with each slider
Theta_slider.on_changed(update)
L_slider.on_changed(update)
Q_slider.on_changed(update)

# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = fig.add_axes([0.1, 0.15, 0.1, 0.04])
button = Button(resetax, 'Reset', hovercolor='0.975')


def reset(event):
    Theta_slider.reset()
    L_slider.reset()
    Q_slider.reset()
button.on_clicked(reset)

plt.show()
